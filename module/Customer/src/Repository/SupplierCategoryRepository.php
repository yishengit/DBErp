<?php
/**
 * DBERP 进销存系统
 *
 * ==========================================================================
 * @link      http://www.dberp.net/
 * @copyright 北京珑大钜商科技有限公司，并保留所有权利。
 * @license   http://www.dberp.net/license.html License
 * ==========================================================================
 *
 * @author    静静的风 <baron@loongdom.cn>
 *
 */

namespace Customer\Repository;

use Customer\Entity\SupplierCategory;
use Doctrine\ORM\EntityRepository;

class SupplierCategoryRepository extends EntityRepository
{
    /**
     * 获取最大供应商分类id
     * @return float|int|mixed|string
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getMaxSupplierCategoryId()
    {
        $query = $this->getEntityManager()->createQueryBuilder();
        $query->select('MAX(s.supplierCategoryId) as maxSupplierCategoryId')->from(SupplierCategory::class, 's');

        $maxId = $query->getQuery()->getSingleScalarResult();

        return $maxId == null ? 1 : $maxId + 1;
    }
}