<?php
/**
 * DBERP 进销存系统
 *
 * ==========================================================================
 * @link      http://www.dberp.net/
 * @copyright 北京珑大钜商科技有限公司，并保留所有权利。
 * @license   http://www.dberp.net/license.html License
 * ==========================================================================
 *
 * @author    静静的风 <baron@loongdom.cn>
 *
 */

namespace Extend;

use Extend\Controller\Factory\IndexControllerFactory;
use Extend\Controller\IndexController;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
use Extend\Event\ExtendListener;
use Extend\Event\Factory\ExtendListenerFactory;
use Extend\Service\Factory\PluginManagerFactory;
use Extend\Service\PluginManager;
use Extend\View\ExtendHelper;
use Extend\View\Factory\ExtendHelperFactory;
use Laminas\Router\Http\Segment;

return [
    'router' => [
        'routes' => [
            'extend' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/extend[/:action]',
                    'defaults' => [
                        'controller' => IndexController::class,
                        'action'    => 'index'
                    ]
                ]
            ],

        ]
    ],

    'controllers' => [
        'factories' => [
            IndexController::class      => IndexControllerFactory::class
        ]
    ],

    'service_manager' => [
        'factories' => [
            PluginManager::class        => PluginManagerFactory::class,

            ExtendListener::class       => ExtendListenerFactory::class
        ]
    ],

    'listeners' => [
        ExtendListener::class
    ],

    'permission_filter' => include __DIR__ . '/permission.php',

    'controller_plugins' => [
        'factories' => [

        ],
        'aliases'   => [

        ]
    ],

    'view_helpers' => [
        'factories' => [
            ExtendHelper::class => ExtendHelperFactory::class
        ],
        'aliases' => [
            'extendHelper' => ExtendHelper::class
        ],
    ],

    'translator' => [
        'translation_file_patterns' => [
            [
                'type' => 'gettext',
                'base_dir' => __DIR__ . '/../data/language',
                'pattern' => '%s.mo'
            ],
            [
                'type' => 'gettext',
                'base_dir' => __DIR__ . '/../data/language/extend',
                'pattern' => '%s.mo'
            ]
        ]
    ],

    'view_manager' => [
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
        'strategies' => [
            'ViewJsonStrategy',
        ]
    ],

    'doctrine' => [
        'driver' => [
            __NAMESPACE__ . '_driver' => [
                'class' => AnnotationDriver::class,
                'cache' => 'array',
                'paths' => [__DIR__ . '/../src/Entity']
            ],
            'orm_default' => [
                'drivers' => [
                    __NAMESPACE__ . '\Entity' => __NAMESPACE__ . '_driver'
                ]
            ]
        ]
    ]
];