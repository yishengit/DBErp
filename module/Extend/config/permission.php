<?php
/**
 * DBERP 进销存系统
 *
 * ==========================================================================
 * @link      http://www.dberp.net/
 * @copyright 北京珑大钜商科技有限公司，并保留所有权利。
 * @license   http://www.dberp.net/license.html License
 * ==========================================================================
 *
 * @author    静静的风 <baron@loongdom.cn>
 *
 */

namespace Extend;

use Extend\Controller\IndexController;

return [
    'Extend' => [
        'name' => '扩展',
        'controllers' => [
            IndexController::class => [
                'name' => '扩展插件',
                'action' => ['index', 'pluginList'],
                'actionNames' => [
                    'index'     => '已安装插件列表',
                    'pluginList'=> '可安装插件列表'
                ]
            ],

        ]
    ]
];
