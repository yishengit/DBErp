<?php
/**
 * DBERP 进销存系统
 *
 * ==========================================================================
 * @link      http://www.dberp.net/
 * @copyright 北京珑大钜商科技有限公司，并保留所有权利。
 * @license   http://www.dberp.net/license.html License
 * ==========================================================================
 *
 * @author    静静的风 <baron@loongdom.cn>
 *
 */

namespace Extend\View;

use Laminas\Mvc\I18n\Translator;
use Laminas\View\Helper\AbstractHelper;

class ExtendHelper extends AbstractHelper
{
    private $translator;

    public function __construct(
        Translator  $translator
    )
    {
        $this->translator   = $translator;
    }

    /**
     * 插件状态
     * @param $state
     * @return mixed
     */
    public function pluginState($state)
    {
        return $state == 1 ? '<span class="text-bold text-green">'.$this->translator->translate('已启用').'</span>' : '<span class="text-bold text-red">'.$this->translator->translate('未启用').'</span>';
    }
}