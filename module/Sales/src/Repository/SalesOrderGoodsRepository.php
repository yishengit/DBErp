<?php
/**
 * DBERP 进销存系统
 *
 * ==========================================================================
 * @link      http://www.dberp.net/
 * @copyright 北京珑大钜商科技有限公司，并保留所有权利。
 * @license   http://www.dberp.net/license.html License
 * ==========================================================================
 *
 * @author    静静的风 <baron@loongdom.cn>
 *
 */

namespace Sales\Repository;

use Doctrine\ORM\EntityRepository;
use Sales\Entity\SalesOrderGoods;

class SalesOrderGoodsRepository extends EntityRepository
{
    /**
     * 根据orderId获取列表信息
     * @param array $orderId
     * @return float|int|mixed|string
     */
    public function findOrderIdGoodsList(array $orderId)
    {
        $query = $this->getEntityManager()->createQueryBuilder();
        $query->select('g')
            ->from(SalesOrderGoods::class, 'g')
            ->where($query->expr()->in('g.salesOrderId', $orderId));

        return $query->getQuery()->getResult();
    }
}