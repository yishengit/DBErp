<?php
/**
 * DBERP 进销存系统
 *
 * ==========================================================================
 * @link      http://www.dberp.net/
 * @copyright 北京珑大钜商科技有限公司，并保留所有权利。
 * @license   http://www.dberp.net/license.html License
 * ==========================================================================
 *
 * @author    静静的风 <baron@loongdom.cn>
 *
 */

namespace Stock\Controller;

use Admin\Data\Common;
use Doctrine\ORM\EntityManager;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\Mvc\I18n\Translator;
use Purchase\Service\PurchaseGoodsPriceLogManager;
use Stock\Entity\OtherWarehouseOrder;
use Stock\Entity\OtherWarehouseOrderGoods;
use Stock\Form\OtherOrderSearchForm;
use Stock\Form\OtherWarehouseOrderForm;
use Stock\Form\OtherWarehouseOrderGoodsForm;
use Stock\Service\OtherWarehouseOrderGoodsManager;
use Stock\Service\OtherWarehouseOrderManager;
use Store\Entity\Goods;

/**
 * 其他入库
 */
class IndexController extends AbstractActionController
{
    private $translator;
    private $entityManager;
    private $otherWarehouseOrderManager;
    private $otherWarehouseOrderGoodsManager;
    private $purchaseGoodsPriceLogManager;

    public function __construct(
        Translator $translator,
        EntityManager $entityManager,
        OtherWarehouseOrderManager $otherWarehouseOrderManager,
        OtherWarehouseOrderGoodsManager $otherWarehouseOrderGoodsManager,
        PurchaseGoodsPriceLogManager $purchaseGoodsPriceLogManager
    )
    {
        $this->translator       = $translator;
        $this->entityManager    = $entityManager;
        $this->otherWarehouseOrderManager       = $otherWarehouseOrderManager;
        $this->otherWarehouseOrderGoodsManager  = $otherWarehouseOrderGoodsManager;
        $this->purchaseGoodsPriceLogManager     = $purchaseGoodsPriceLogManager;
    }

    /**
     * 其他入库列表
     * @return array
     */
    public function indexAction(): array
    {
        $array= [];

        $page = (int) $this->params()->fromQuery('page', 1);

        $search = [];
        $searchForm = new OtherOrderSearchForm();
        $searchForm->get('warehouse_id')->setValueOptions($this->storeCommon()->warehouseListOptions());
        if($this->getRequest()->isGet()) {
            $data = $this->params()->fromQuery();
            $searchForm->setData($data);
            if($searchForm->isValid()) $search = $searchForm->getData();
        }
        $array['searchForm'] = $searchForm;
        $query = $this->entityManager->getRepository(OtherWarehouseOrder::class)->findOtherWarehouseOrderList($search);
        $array['orderList'] = $this->adminCommon()->erpPaginator($query, $page);

        return $array;
    }

    /**
     * 添加其他入库
     * @return array|\Laminas\Http\Response
     */
    public function addAction()
    {
        $goodsSubmitList = [];

        $goodsForm  = new OtherWarehouseOrderGoodsForm($this->entityManager);
        $form       = new OtherWarehouseOrderForm($this->entityManager);

        $form->get('warehouseId')->setValueOptions($this->storeCommon()->warehouseListOptions());

        if($this->getRequest()->isPost()) {
            $data = $this->params()->fromPost();
            $form->setData($data);
            $goodsForm->setData($data);
            if($form->isValid() && $goodsForm->isValid()) {
                $data = $form->getData();
                $goodsData = $goodsForm->getData();

                $this->entityManager->beginTransaction();
                try {
                    $otherWarehouseOrder = $this->otherWarehouseOrderManager->addOtherWarehouseOrder($data, $goodsData, $this->adminSession('admin_id'));
                    $this->otherWarehouseOrderGoodsManager->addOtherWarehouseOrderGoods($goodsData, $data['warehouseId'], $otherWarehouseOrder->getOtherWarehouseOrderId());

                    if ($data['syncPrice'] == 1) {//同步商品单价到采购单价
                        $this->purchaseGoodsPriceLogManager->addOtherPurchaseGoodsPriceLog($goodsData);
                    }

                    $this->getEventManager()->trigger('other-warehouse-order.insert.post', $this, ['otherWarehouseOrder' => $otherWarehouseOrder, 'syncPrice' => $data['syncPrice']]);

                    $this->entityManager->commit();

                    $message = $otherWarehouseOrder->getWarehouseOrderSn() . $this->translator->translate('其他入库成功！');
                    $this->adminCommon()->addOperLog($message, $this->translator->translate('其他入库'));
                } catch (\Exception $e) {
                    $this->entityManager->rollback();
                    $this->flashMessenger()->addWarningMessage($this->translator->translate('其他入库失败！'));
                }
                return $this->redirect()->toRoute('erp-stock');
            } else {
                $goodsData = $goodsForm->getData();
                if (is_array($goodsData['goodsId']) && !empty($goodsData['goodsId'])) {
                    foreach ($goodsData['goodsId'] as $key => $value) {
                        $goodsSubmitList[$value] = [
                            'goodsId' => $value,
                            'goodsPrice' => $data['goodsPrice'][$key],
                            'goodsTax' => $data['goodsTax'][$key],
                            'goodsBuyNum' => $data['goodsBuyNum'][$key],
                            'goodsAmount' => $data['goodsAmount'][$key],
                            'goodsSerialNumber' => $data['goodsSerialNumber'][$value]??''
                        ];
                    }

                    $goodsSubmitArray = $this->entityManager->getRepository(Goods::class)->findGoodsArray($goodsData['goodsId']);
                    foreach ($goodsSubmitArray as $goodsValue) {
                        if (isset($goodsSubmitList[$goodsValue[0]['goodsId']])) {
                            $goodsSubmitList[$goodsValue[0]['goodsId']]['goodsName'] = $goodsValue[0]['goodsName'];
                            $goodsSubmitList[$goodsValue[0]['goodsId']]['goodsSpec'] = $goodsValue[0]['goodsSpec'];
                            $goodsSubmitList[$goodsValue[0]['goodsId']]['unitName']  = $goodsValue['unitName'];
                            $goodsSubmitList[$goodsValue[0]['goodsId']]['goodsNumber'] = $goodsValue[0]['goodsNumber'];
                            $goodsSubmitList[$goodsValue[0]['goodsId']]['goodsSerialNumberState'] = $goodsValue[0]['goodsSerialNumberState'];
                        }
                    }
                }
            }

        }

        if (empty($data)) $form->setData(['warehouseOrderSn' => $this->codeAndNumber()->createStockInCode()]);

        return [
            'form' => $form,
            'goodsForm' => $goodsForm,
            'goodsSubmitList' => $goodsSubmitList
        ];
    }

    /**
     * 查看其他入库订单信息
     * @return array|\Laminas\Http\Response
     */
    public function viewAction()
    {
        $otherWarehouseOrderId  = (int) $this->params()->fromRoute('id', -1);
        $otherWarehouseOrderInfo= $this->entityManager->getRepository(OtherWarehouseOrder::class)->findOneBy(['otherWarehouseOrderId' => $otherWarehouseOrderId]);
        if($otherWarehouseOrderInfo == null) {
            $this->flashMessenger()->addWarningMessage($this->translator->translate('该入库单不存在！'));
            return $this->redirect()->toRoute('erp-stock');
        }

        $this->layout()->setVariable('titleName', $this->translator->translate('其他入库').'-'.$otherWarehouseOrderInfo->getWarehouseOrderSn().'-'.Common::configValue('base', 'config')['company_name']);

        $orderGoods = $this->entityManager->getRepository(OtherWarehouseOrderGoods::class)->findBy(['otherWarehouseOrderId' => $otherWarehouseOrderId]);

        return ['otherWarehouseOrder' => $otherWarehouseOrderInfo, 'orderGoods' => $orderGoods];
    }
}