<?php
/**
 * DBERP 进销存系统
 *
 * ==========================================================================
 * @link      http://www.dberp.net/
 * @copyright 北京珑大钜商科技有限公司，并保留所有权利。
 * @license   http://www.dberp.net/license.html License
 * ==========================================================================
 *
 * @author    静静的风 <baron@loongdom.cn>
 *
 */

namespace Store\Form;

use Admin\Data\Config;
use Admin\Filter\ImageRegenerate;
use Admin\Filter\OldImage;
use Admin\Validator\ImageSuffixValidator;
use Doctrine\ORM\EntityManager;
use Store\Entity\Goods;
use Store\Validator\GoodsBarCodeValidator;
use Store\Validator\GoodsCodeValidator;
use Laminas\Form\Form;
use Laminas\I18n\Translator\Translator;

class GoodsForm extends Form
{
    private $translator;
    private $entityManager;
    private $goods;

    public function __construct(EntityManager $entityManager = null, Goods $goods = null, $name = 'goods-form', array $options = [])
    {
        parent::__construct($name, $options);

        $this->setAttribute('method', 'post');
        $this->setAttribute('enctype', 'multipart/form-data');
        $this->setAttribute('class', 'form-horizontal');

        $this->translator   = new Translator();
        $this->entityManager= $entityManager;
        $this->goods        = $goods;

        $this->addElements();
        $this->addInputFilter();
    }

    public function addElements()
    {
        $this->add([
            'type'  => 'text',
            'name'  => 'goodsNumber',
            'attributes'    => [
                'id'            => 'goodsNumber',
                'class'         => 'form-control'
            ]
        ]);

        $this->add([
            'type'  => 'text',
            'name'  => 'goodsName',
            'attributes'    => [
                'id'            => 'goodsName',
                'class'         => 'form-control'
            ]
        ]);

        $this->add([
            'type'  => 'file',
            'name'  => 'goodsImage',
            'attributes'    => [
                'id'            => 'goodsImage',
                'class'         => 'form-control'
            ]
        ]);

        $this->add([
            'type'  => 'select',
            'name'  => 'goodsCategoryId',
            'attributes'    => [
                'id'            => 'goodsCategoryId',
                'class'         => 'form-control'
            ]
        ]);

        $this->add([
            'type'  => 'select',
            'name'  => 'brandId',
            'attributes'    => [
                'id'            => 'brandId',
                'class'         => 'form-control'
            ]
        ]);

        $this->add([
            'type'  => 'select',
            'name'  => 'unitId',
            'attributes'    => [
                'id'            => 'unitId',
                'class'         => 'form-control'
            ]
        ]);

        $this->add([
            'type'  => 'text',
            'name'  => 'goodsSpec',
            'attributes'    => [
                'id'            => 'goodsSpec',
                'class'         => 'form-control'
            ]
        ]);

        $this->add([
            'type'  => 'text',
            'name'  => 'goodsStock',
            'attributes'    => [
                'id'            => 'goodsStock',
                'class'         => 'form-control',
                'readonly'      => 'readonly',
                'value'         => 0
            ]
        ]);

        $this->add([
            'type'  => 'text',
            'name'  => 'goodsBarcode',
            'attributes'    => [
                'id'            => 'goodsBarcode',
                'class'         => 'form-control'
            ]
        ]);

        $this->add([
            'type'  => 'textarea',
            'name'  => 'goodsInfo',
            'attributes'    => [
                'id'            => 'goodsInfo',
                'class'         => 'form-control',
                'rows'          => 5
            ]
        ]);

        $this->add([
            'type'  => 'text',
            'name'  => 'goodsRecommendPrice',
            'attributes'    => [
                'id'    => 'goodsRecommendPrice',
                'class' => 'form-control'
            ]
        ]);

        $this->add([
            'type'  => 'text',
            'name'  => 'goodsSort',
            'attributes'    => [
                'id'            => 'goodsSort',
                'class'         => 'form-control',
                'value'         => 255
            ]
        ]);

        $this->add([
            'type'  => 'checkbox',
            'name'  => 'goodsSerialNumberState',
            'attributes' => [
                'value' => 0
            ]
        ]);

        for($i=1; $i<=10; $i++) {
            $this->add([
                'type'  => 'text',
                'name'  => 'customTitle'.$i,
                'attributes'    => [
                    'id'            => 'customTitle'.$i,
                    'class'         => 'form-control'
                ]
            ]);
            $this->add([
                'type'  => 'text',
                'name'  => 'customContent'.$i,
                'attributes'    => [
                    'id'            => 'customContent'.$i,
                    'class'         => 'form-control'
                ]
            ]);
        }

    }

    public function addInputFilter()
    {
        $inputFilter = $this->getInputFilter();

        $inputFilter->add([
            'name'      => 'goodsNumber',
            'required'  => true,
            'filters'   => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags']
            ],
            'validators'=> [
                [
                    'name'      => 'StringLength',
                    'options'   => [
                        'min'   => 1,
                        'max'   => 20
                    ]
                ],
                [
                    'name'      => GoodsCodeValidator::class,
                    'options'   => [
                        'entityManager' => $this->entityManager,
                        'goods'         => $this->goods
                    ]
                ]
            ]
        ]);

        $inputFilter->add([
            'name'      => 'goodsName',
            'required'  => true,
            'filters'   => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags']
            ],
            'validators'=> [
                [
                    'name'      => 'StringLength',
                    'options'   => [
                        'min'   => 1,
                        'max'   => 100
                    ]
                ]
            ]
        ]);

        $inputFilter->add([
            'type'      => 'Laminas\InputFilter\FileInput',
            'name'      => 'goodsImage',
            'required'  => false,
            'validators'=> [
                ['name' => 'FileUploadFile'],
                ['name' => ImageSuffixValidator::class],
                [
                    'name' => 'FileMimeType',
                    'options' => [
                        'mimeType' => ['image/jpeg', 'image/png']
                    ]
                ],
                ['name' => 'FileIsImage'],
                [
                    'name' => 'FileSize',
                    'options' => [
                        'min' => '1kB',
                        'max' => '8MB'
                    ]
                ]
            ],
            'filters'  => [
                [
                    'name' => 'FileRenameUpload',
                    'options' => [
                        'target' => 'public/upload/goods/',
                        'useUploadName'=>true,
                        'useUploadExtension'=>true,
                        'overwrite'=>true,
                        'randomize'=>true
                    ]
                ],
                ['name' => ImageRegenerate::class],
                [
                    'name' => OldImage::class,
                    'options' => [
                        'oldImage' => $this->goods ? $this->goods->getGoodsImage() : ''
                    ]
                ]
            ]
        ]);

        $inputFilter->add([
            'name'      => 'goodsBarcode',
            'required'  => false,
            'filters'   => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags']
            ],
            'validators'=> [
                [
                    'name'      => 'StringLength',
                    'options'   => [
                        'min'   => 1,
                        'max'   => 30
                    ]
                ],
                [
                    'name'      => GoodsBarCodeValidator::class,
                    'options'   => [
                        'entityManager' => $this->entityManager,
                        'goods'         => $this->goods
                    ]
                ]
            ]
        ]);

        $inputFilter->add([
            'name'      => 'goodsSpec',
            'required'  => false,
            'filters'   => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags']
            ],
            'validators'=> [
                [
                    'name'      => 'StringLength',
                    'options'   => [
                        'min'   => 1,
                        'max'   => 50
                    ]
                ]
            ]
        ]);

        $inputFilter->add([
            'name'      => 'goodsInfo',
            'required'  => false,
            'filters'   => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags']
            ],
            'validators'=> [
                [
                    'name'      => 'StringLength',
                    'options'   => [
                        'min'   => 1,
                        'max'   => 500
                    ]
                ]
            ]
        ]);

        $inputFilter->add([
            'name'      => 'goodsCategoryId',
            'required'  => true,
            'filters'   => [
                ['name' => 'ToInt']
            ],
            'validators'=> [
                [
                    'name'      => 'GreaterThan',
                    'options'   => [
                        'min'   => 0
                    ]
                ]
            ]
        ]);

        $inputFilter->add([
            'name'      => 'unitId',
            'required'  => true,
            'filters'   => [
                ['name' => 'ToInt']
            ],
            'validators'=> [
                [
                    'name'      => 'GreaterThan',
                    'options'   => [
                        'min'   => 0
                    ]
                ]
            ]
        ]);

        $inputFilter->add([
            'name'      => 'goodsRecommendPrice',
            'required'  => true,
            'validators'=> [
                [
                    'name' => 'GreaterThan',
                    'options' => [
                        'min' => 0
                    ]
                ]
            ],
            'error_message' => $this->translator->translate('建议售价必须大于0')
        ]);

        $inputFilter->add([
            'name'      => 'brandId',
            'required'  => true,
            'filters'   => [
                ['name' => 'ToInt']
            ],
            'validators'=> [
                [
                    'name'      => 'GreaterThan',
                    'options'   => [
                        'min'   => -1
                    ]
                ]
            ]
        ]);

        $inputFilter->add([
            'name'      => 'goodsSerialNumberState',
            'required'  => false,
            'validators'=> [
                [
                    'name'      => 'InArray',
                    'options'   => [
                        'haystack'  => [0, 1]
                    ]
                ]
            ]
        ]);

        for($i=1; $i<=10; $i++) {
            $inputFilter->add([
                'name'      => 'customTitle'.$i,
                'required'  => false,
                'filters'   => [
                    ['name' => 'StringTrim'],
                    ['name' => 'StripTags']
                ],
                'validators'=> [
                    [
                        'name'      => 'StringLength',
                        'options'   => [
                            'min'   => 1,
                            'max'   => 50
                        ]
                    ]
                ]
            ]);
            $inputFilter->add([
                'name'      => 'customContent'.$i,
                'required'  => false,
                'filters'   => [
                    ['name' => 'StringTrim'],
                    ['name' => 'StripTags']
                ],
                'validators'=> [
                    [
                        'name'      => 'StringLength',
                        'options'   => [
                            'min'   => 1,
                            'max'   => 200
                        ]
                    ]
                ]
            ]);
        }
    }
}